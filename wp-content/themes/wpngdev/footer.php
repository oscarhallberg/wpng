<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wPnGdEV
 */

?>

	<footer id="colophon" class="site-footer" role="contentinfo">
  	<div id="contact">
    	
      <div class="shape contactheadershape vertical-align-container-def">
        <div class="def">
          <h1>Contact</h1>
        </div>
      </div>    	
  	
      <div class="shape contactshape vertical-align-container-def">
        <div class="def">
          <?php echo do_shortcode( '[contact-form-7 id="33" title="Footercontact"]' ); ?>
        </div>
        <div class="contactdetails">
          <a href="tel:+447847029403">+447847029403</a><br>
          <a href="tel:+46708380022">+46708380022</a><br>
          <a href="https://uk.linkedin.com/in/oscar-hallberg-70a46413" target="_blank">LinkedIn</a>
        </div>
      </div>
    
    </div>
    
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
