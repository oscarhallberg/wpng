<?php
/**
 * wPnGdEV functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wPnGdEV
 */

if ( ! function_exists( 'wpngdev_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wpngdev_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wPnGdEV, use a find and replace
	 * to change 'wpngdev' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wpngdev', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'wpngdev' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wpngdev_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'wpngdev_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wpngdev_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wpngdev_content_width', 640 );
}
add_action( 'after_setup_theme', 'wpngdev_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wpngdev_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wpngdev' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wpngdev' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wpngdev_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wpngdev_scripts() {
	wp_enqueue_style( 'wpngdev-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'normalise', get_template_directory_uri() . '/css/normalize.css' );
	wp_enqueue_style( 'fonts', get_template_directory_uri() . '/css/fonts/fonts.css' );
	wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css' );
	wp_enqueue_style( 'grids', get_template_directory_uri() . '/css/grids.css' );
	wp_enqueue_style( 'design', get_template_directory_uri() . '/css/design.css' );
	wp_enqueue_style( 'misc', get_template_directory_uri() . '/css/misc.css' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css' );
	wp_enqueue_style( 'print', get_template_directory_uri() . '/css/print.css' );

	wp_enqueue_script( 'wpngdev-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'wpngdev-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
  wp_deregister_script( 'jquery' );
  $jquery_cdn = '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
  wp_enqueue_script( 'jquery', $jquery_cdn, array(), '20130115', true );
  
	wp_enqueue_script( 'skel1', get_template_directory_uri() . '/js/skel1.js', array('slick'), '20170925', true );  
  
	wp_enqueue_script( 'customscripts', get_template_directory_uri() . '/js/scripts.js', array('slick'), '20160105', true );
	
  wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '20160104', true ); 
  
  wp_enqueue_script( 'ngslick', get_template_directory_uri() . '/js/ngslick.min.js', array('jquery'), '09460104', true );	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpngdev_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

function my_custom_login() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.css" />';
}
add_action('login_head', 'my_custom_login');
