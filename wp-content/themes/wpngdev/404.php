<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package wPnGdEV
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
  		<div class="centered-content">

			<section class="error-404 not-found">
				<div class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Nothing found.', 'wpngdev' ); ?></h1>
				</div><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'Nothing was found here.', 'wpngdev' ); ?><a href="<?php bloginfo('url')?>">Try the home page?</a></p>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

  		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
