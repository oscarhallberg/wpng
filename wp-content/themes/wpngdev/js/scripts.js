$(document).ready(function(){
  
/*
>>>>>>>>>>>>>>>>
Custom scripts
>>>>>>>>>>>>>>>>
*/

  /*
  >>>>>>>>>>>>>>>>
  Trigger scripts on load and resize
  >>>>>>>>>>>>>>>>
  */
  
  $(window).on('load resize',function(e){
  
    // *** Call function to fit images to parent ***  
  //   $('img').skeL1imageFitter();
 
    // *** Set displayed image to its original size ***  
  //   $( 'img').skeL1realSize();
  
    // *** Call function for Sticky Footer ***   
    $('.page-footer').skeL1stickyFooter();
    
    // *** Call function for Golden Ratio Height *** 
    $('.goldenratio').skeL1goldenRatio();
    
  });  
  
  /*
  ^^^^^^^^^^^^^^^^
  END Trigger scripts on load and resize
  <<<<<<<<<<<<<<<<
  */
    
  
  // Toggle menu on smaller viewports
    $('.burger-menu').skeL1mobileNav();  

  // Modals    
    $('.some-class').click(function(){
      $(this).skeL1modal();
      $('.a-modal').toggleClass('toggled centered');
    });
  
  // *** Call function for Sticky Header ***  
    $('header').skeL1stickyHeader();
    
  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });
  
//     FOCUS CONTACT FORM

  $('.fffield').each(function(){
    $('input, textarea').focus(function(){
      $(this).closest('.fffield').removeClass('filled')
      $(this).closest('.fffield').addClass('focused');
    })
  });
  
  $('.fffield input, .fffield textarea').blur(function(){                   
    if( !$(this).val() ) { 
      $(this).closest('.fffield').removeClass('focused');
    } else {
      $(this).closest('.fffield').addClass('filled');
    }
  });
  
  $('.fffield input').click(function(){
    $(this).closest('.fffield').find('span.wpcf7-not-valid-tip').addClass('hiding');
  });
  
  $('.fffield input, .fffield textarea').on('click', function () {
    $(this).select();
  });
    
//     END FOCUS CONTACT FORM
        
  
/*
<<<<<<<<<<<<<<<<
END scripts
<<<<<<<<<<<<<<<<
*/  

$(".hide-on-load").each(function(){
  $(this).css("opacity", "1");   
});
 


// Close docReady
});