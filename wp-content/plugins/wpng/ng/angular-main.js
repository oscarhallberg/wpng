var wpNgApp = new angular.module( 'wpNg', ['ui.router', 'ngResource', 'slick'] );

function GlobalController($scope, $http) {
  
  $http.get(appInfo.rest_api_url + 'utility').then(function(response) {
    $scope.globalposts = response.data;
  });  
  
}

wpNgApp.controller('GlobalController', GlobalController);

wpNgApp.factory( 'Posts', function( $resource ) {
	return $resource( appInfo.rest_api_url + 'portfolio/:ID', {
		ID: '@id'
	})
});

wpNgApp.controller( 'ListCtrl', ['$scope', 'Posts', '$http', function( $scope, Posts, $http ) {
	$scope.page_title_first = 'Oscar';
	$scope.page_title_last = 'Hallberg';
	$scope.rootjson = appInfo.rest_api_url;
	var ngJson_properties = appInfo.rest_api_url + 'portfolio?per_page=100';
	
  $http.get(ngJson_properties).then(function(response) {
    $scope.frontposts = response.data;
  });
	
	Posts.query(function( res ) {
		$scope.posts = res;
		
	});


	
}]);

wpNgApp.controller( 'PrintCtrl', ['$scope', 'Posts', function( $scope, Posts ) {

	$scope.rootjson = appInfo.rest_api_url;

	Posts.query(function( res ) {
		$scope.posts = res;
		
	});
	
}]);

wpNgApp.controller( 'DetailCtrl', ['$scope', '$stateParams', 'Posts', function( $scope, $stateParams, Posts ) {
	Posts.get( { ID: $stateParams.id}, function(res){
		$scope.post = res;
	})
}])

wpNgApp.config( function( $stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state( 'list', {
			url: '/',
			controller: 'ListCtrl',
			templateUrl: appInfo.template_directory + 'html/list.html'
		})
		.state( 'detail', {
			url: '/posts/:id',
			controller: 'DetailCtrl',
			templateUrl: appInfo.template_directory + 'html/detail.html'
		})
		.state( 'printcv', {
			url: '/print',
			controller: 'PrintCtrl',
			templateUrl: appInfo.template_directory + 'html/print.html'
		})
});

wpNgApp.filter( 'its_trusted', ['$sce', function( $sce ){
	return function( text ) {
		return $sce.trustAsHtml( text );
	}
}])

wpNgApp.directive('scrollOnClick', function() {
  return {
    restrict: 'A',
    link: function(scope, $elm, attrs) {
      var idToScroll = attrs.href;
      $elm.on('click', function() {
        var $target;
        if (idToScroll) {
          $target = $(idToScroll);
        } else {
          $target = $elm;
        }
        $("body").animate({scrollTop: $target.offset().top}, "slow");
      });
    }
  }
});
