<?php
/*
Plugin Name: wPnG
Description: Puts AngularJS in controll.
Version: 0.1
Author: OH
License: GPL
Copyright: OH
*/



// Add scripts
function wp_ng_scripts() {
  
		wp_enqueue_script( 'angularjsmin', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js', array( 'jquery' ), '1.0', false );
		wp_enqueue_script( 'angularresource', '//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-resource.js', array('angularjsmin'), '1.0', false );
		wp_enqueue_script( 'uirouter', 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js', array( 'angularjsmin' ), '1.0', false );
		wp_enqueue_script( 'ngMain', plugin_dir_url( __FILE__ ) . '/ng/angular-main.js', array( 'uirouter' ), '1.0', false );
		wp_enqueue_script( 'ngRealted', plugin_dir_url( __FILE__ ) . '/js/ng-related.js', array( 'ngMain' ), '1.0', false );
		wp_localize_script( 'ngMain', 'appInfo',
			array(
				
				'rest_api_url'			 => rest_get_url_prefix() . '/wp/v2/',
				'template_directory' => get_template_directory_uri() . '/',
				'plugin_directory' => plugin_dir_url( __FILE__ ) . '/'
				
			)
		);
    
}
add_action( 'wp_enqueue_scripts', 'wp_ng_scripts' );