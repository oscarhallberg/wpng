<?php
/*
Plugin Name: OH Backend
Description: Project Specific Fields and Content Types
Version: 0.1
Author: OH
License: GPL
Copyright: BD
*/

// Files
include( plugin_dir_path( __FILE__ ) . 'site-post-types.php');
include( plugin_dir_path( __FILE__ ) . 'site-image-sizes.php');
include( plugin_dir_path( __FILE__ ) . 'site-custom-fields.php');