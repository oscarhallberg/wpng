<?php 
  
//Image sizes
add_action( 'after_setup_theme', 'theme_setup' );

function theme_setup() {
      if ( function_exists( 'add_theme_support' ) ) {
        add_image_size( 'homehero', 2400, 1320, true );   
        add_image_size( 'portfolio', 600, 1600, false );
        add_image_size( 'singleportfolio', 1800, 2400, false );     
    }
}

add_filter( 'jpeg_quality', create_function( '', 'return 60;' ) );