# wPnG #

Plugin for WordPress Angular JS

## Use ##

In theme folder add folder _html_ with contents. This is the templates for each view.

## index.php int theme ##

    get_header(); ?>
    
    	<div id="primary" class="content-area">
    		<main id="main" class="site-main" role="main">
    
    <div ui-view></div>
    
    		</main><!-- #main -->
    	</div><!-- #primary -->
    
    <?php
    get_footer();